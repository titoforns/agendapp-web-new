$(document).ready(function() {
    $('.grid__item .grid__item-wrap').on('click', function(e) {
        e.stopPropagation();
        e.preventDefault();
        
        $('.content__item').hide();

        let id = $(this).parent().attr('idcontent');

        showContentOfLink(id);

        $('#item-' + id).show();


    });

    $('.grid__item').on('click', function(e) {
        e.stopPropagation();
        e.preventDefault();
    });

    $('.content__close').on('click', function(e) {
        e.stopPropagation();
        e.preventDefault();
        console.error(e)
        hideContents();
        $('.content__item').hide();
        $('.grid__item').show();
    })
})

function showContentOfLink(id) {
    hideContents();
    $('#link-' + id).show();
    $('#link-' + id).addClass('showing-data');
}

function hideContents() {
    $('.grid__item').hide();
    $('.grid__item').removeClass('showing-data');
}