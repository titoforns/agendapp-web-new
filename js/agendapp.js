$('.select-tab').on('click', function(e) {
    $('.select-tab').removeClass('selected')
    $(this).addClass('selected');
    let id = $(this).attr('id');
    if(id == 'professional') {
        $('#professional-form').show();
        $('#center-form').hide();
    } else {
        $('#professional-form').hide();
        $('#center-form').show();
    }
})

$(document).ready(()=>{
    $('#conocenos-selector-1').click(()=>{
        deselectAllItemsConocenos();
        $('#conocenos-selector-1').addClass('selected');
        $('#conocenos-image-1').show();
    });

    $('#conocenos-selector-2').click(()=>{
        deselectAllItemsConocenos();
        $('#conocenos-selector-2').addClass('selected');
        $('#conocenos-image-2').show();
    });

    $('#conocenos-selector-3').click(()=>{
        deselectAllItemsConocenos();
        $('#conocenos-selector-3').addClass('selected');
        $('#conocenos-image-3').show();
    });

    $('#conocenos-selector-4').click(()=>{
        deselectAllItemsConocenos();
        $('#conocenos-selector-4').addClass('selected');
        $('#conocenos-image-4').show();
    });

    $('#conocenos-selector-5').click(()=>{
        deselectAllItemsConocenos();
        $('#conocenos-selector-5').addClass('selected');
        $('#conocenos-image-5').show();
    });
})

function deselectAllItemsConocenos() {
    $('.conocenos-item').removeClass('selected');
    $('.conocenos-image').hide();
}