
var categorySelected;
var categories;
const postUrl = "https://ms2.pragmadesarrollos.com/api/professionalsData/list";
const getUrl = "https://ms2.pragmadesarrollos.com/api/professionalsData/filters";


function orderArray(items, prop) {
    items.sort(function (a, b) {
      if (a[prop] > b[prop]) {
        return 1;
      }
      if (a[prop] < b[prop]) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
    return items;
}

async function  loadCategories() {
    const res = await fetch(getUrl);
    const data = await res.json();
    if(data) {
        setCategories(data.result.categories);
    }
}

function setCategories(cats) {
    categories = cats;
    let html = '<option value="Todas" data="-1" idCat="-1" class="dropdown-item" >Todas</option>';
    cats.forEach( (c, i) => {
        html += '<option value="' + c.name  + '" data="' + c.name + '" idCat="' + c.uid + '" class="dropdown-item" >' + c.name + '</option>'
    })
    $('.btn-dropdown').html(html);
}

const loadData = async () => {
    loading(true);
    loadCategories();
    loading(false);
}

$('#btn-search').click(()=> {
    sendData();
});

$('.filters-search').on('keyup', function(e) {
    e.preventDefault();
    e.stopPropagation();
    if(e.keyCode == 13) {
        
        sendData();
    }
})

function loading(b) {
    if(b) {
        var preloader = $('#loading'),
			loader = preloader.find('#loading-center-absolute');
			loader.show();
			preloader.fadeIn('fast');
    } else {
        var preloader = $('#loading'),
			loader = preloader.find('#loading-center-absolute');
			loader.fadeOut();
			preloader.delay(400).fadeOut('slow');
    }
}

function sendData() {
    loading(true);
    let obj = {
        name: null,
        medicalInsurances: [],
        categories: [],
        radio: null,
        center: {
            lat: null,
            lng: null
        }
    }
    
    obj.name = getNameOfProfessional();

    let categorySelected = getCategorySelected();
    console.error(categorySelected)

    if(categorySelected && categorySelected != 'Todas') {
        obj.categories.push(getCategoryIdByName(categorySelected));
    }

    sendAjax(obj);
}

function getCategorySelected() {
    return $('.btn-dropdown').val()
}

function sendAjax(obj) {
    console.error(obj);
    $.ajax({
      type: "POST",
      url: postUrl,
      data: JSON.stringify(obj),
      success: function(e) {
          console.error(e, obj)
          localStorage.setItem('searchObj', JSON.stringify(obj));
          localStorage.setItem('filteredItems', JSON.stringify(e.result));
          window.location.assign('search.html')
      },
      contentType: 'application/json',
      error: function (e) {
          console.error(e);
          sendErrorToast(e);
      }
    });
}

function sendErrorToast(error) {
    $('.toast-agendapp').addClass('show');
    setTimeout(()=> {
        closeToast();
    }, 4000);
}

function closeToast(error) {
    $('.toast-agendapp').removeClass('show');
}

function getNameOfProfessional() {
    return $('#professional_name').val();
}

function parseObj(obj) {
    if(!obj) {
        return;
    }
    if(obj.name) {
        $('#professional_name').val(obj.name);
    }
    if(obj.categories) {
        obj.categories = getCategoryIdByName(obj.categories);
    } else {
        obj.categories = [];
    }
    return obj;
}

function getCategoryIdByName(name) {
    let cat = categories.find(c => c.name.toLowerCase() == name.toLowerCase());
    return cat.uid;
}

function getCategoryByName(name) {
    return categories.find(c => c.name.toLowerCase() == name.toLowerCase());
}

loadData();

