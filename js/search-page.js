
var categories;
var insurances;
const postUrl = "https://ms2.pragmadesarrollos.com/api/professionalsData/list";
const getUrl = "https://ms2.pragmadesarrollos.com/api/professionalsData/filters";
var allProfessionals;
const urlApiProvinces = 'https://apis.datos.gob.ar/georef/api/provincias';
const urlApiCiudades = 'https://apis.datos.gob.ar/georef/api/municipios?provincia=';
var locations;
var ciudades;
// https://apis.datos.gob.ar/georef/api/municipios?provincia=22&campos=id,nombre&max=100

async function loadData() {
    loading(true);
    let searchObj = JSON.parse(localStorage.getItem('searchObj'));
    let filteredItems = JSON.parse(localStorage.getItem('filteredItems'));
    
    const p = await loadAllProfessionals();
    const c = await loadCategories();
    const l = await loadLocationData();

    if(c) {
        setCategories(c.result.categories);
        setInsurances(c.result.medicalInsurances);
    }

    if(l) {
        setLocations(orderArray(l, 'nombre'));
    }

    if(searchObj) {
        setProfessionals(orderArray(filteredItems, 'name'))
    }
    loading(false);
}

function setProfessionals(profs) {
    setFilteredClass();
    let html = '';
    profs.forEach((p, i) => {
        if(p.approved) {
            html += `
                <div class="project-details masonry-image col-md-4">
                    <div class="card-wrapper">
                        <div class="project-preview rel">
                            <div class="hover-overlay"> 
                            <img src="${parsePhoto(p.photo)}" alt="project-preview" />
                            <div class="item-overlay"></div>
                        </div>
                    </div>	
                    <div class="project-txt">
                        <p class="p-md grey-color job">${p.location ? p.location.address + ', ' + p.location.city + ', ' + p.location.country : 'Sin direccion cargada'}</p>
                        <h5 class="h5-lg">
                            ${p.name || 'Sin nombre'}
                        </h5>

                        <div class="project-rating clearfix ico-20">
                            
                            <div class="other-data">
                                ${getInsurances(p.medicalInsurance)}
                            </div>
                        </div>
                        
                        <div class="actions-wrapper animated fadeInUp">
                            <button class="profile-btn" onclick="goProfile('${p.uid}');"  >Ver perfil</button>
                            <button class="turn-btn" onclick="login('${p.uid}');"  >Pedir turno</button>
                        </div>
                    </div>	
                    </div>

                </div>


          
            `;
        }
    });
    $('#cards').html(html);
    if(!profs || profs.length == 0) {
        setAclarations(false);
    } else {
        setAclarations(true);
    }
}

function setAclarations(b) {
    if(b) {
        $('.aclarations').html('Esta es la lista de profesionales que hemos encontrado segun tu busqueda!')
    } else {
        $('.aclarations').html('No hemos encontrado ningun profesional con esos filtros de busqueda! :(')
    }
}

function setFilteredClass() {
    $('body').addClass('searched')
}

const loadAllProfessionals = async () => {
    let obj = {
        name: null,
        medicalInsurances: [],
        categories: [],
        radio: null,
        center: {
            lat: null,
            lng: null
        }
    }
    return new Promise((resolve, reject) => {
        $.ajax({
          type: "POST",
          url: postUrl,
          data: JSON.stringify(obj),
          success: function(e) {
              if(e && e.result) {
                  allProfessionals = e.result;
                  resolve(true);
              } else {
                  reject(false);
              }
          },
          contentType: 'application/json',
          error: function (e) {
              console.error(e);
              reject(false);
          }
        });
    })
} 

const loadCategories = async () => {
    const res = await fetch(getUrl);
    const data = await res.json();
    console.error(data);
    return data;
}

const loadLocationData = async () => {
    const resLocations = await fetch(urlApiProvinces);
    const dataLocations = await resLocations.json();
    return filterProvinces(dataLocations.provincias);
}

function setCategories(cats) {
    categories = cats;
    let html = '<option class="dropdown-item" >Todas las categorías</option>';
    cats.forEach(c => {
        html += '<option class="dropdown-item">' + c.name + '</option>'
    })
    $('#categories').html(html);
}

function setInsurances(ins) {
    console.warn(ins)
    insurances = ins;
    let html = '<option class="dropdown-item" href="#">Todas las obras sociales</option>';
    ins.forEach(c => {
        html += '<option class="dropdown-item">' + c.name + '</option>'
    })
    $('#insurances').html(html);
}

function setLocations(locs) {
    locations = locs;
    let html = '<option class="dropdown-item">Cualquier provincia</option>';
    locs.forEach(c => {
        html += '<option class="dropdown-item" href="#">' + c.nombre + '</option>'
    })
    $('#locations').html(html);
    setCityDropdownEnable(false);
}

function orderArray(items, prop) {
    items.sort(function (a, b) {
      if (a[prop] > b[prop]) {
        return 1;
      }
      if (a[prop] < b[prop]) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
    return items;
}

function loading(b) {
    console.error(b)
    if(b) {
        var preloader = $('#loading'),
			loader = preloader.find('#loading-center-absolute');
			loader.show();
			preloader.fadeIn('fast');
    } else {
        var preloader = $('#loading'),
			loader = preloader.find('#loading-center-absolute');
			loader.fadeOut();
			preloader.delay(400).fadeOut('slow');
    }
}

function filterProvinces(provs) {
    let filtereds = [];
    provs.forEach(p => {
        let finded = false;
        allProfessionals.forEach(pr => {
            if(pr.location && pr.location.province === p.nombre){
                finded = true;
            }
        });
        if(finded) {
            filtereds.push(p);
        }
    });
    return filtereds;
}

function setCityDropdownEnable(b) {
    if(b) {
        $('#citys').removeClass('disabled');
    } else {
        $('#citys').addClass('disabled');
        $('#citys').find('button').html('Cualquier ciudad');
        ciudadSelected = null;
    }
}

loadData();


// Fin de la carga inicial


//Carga de ciudades cuando cambia la provincia

async function setProvince(name) {
    if(name == 'Cualquier provincia') {
        $('#citys').addClass('disabled');
        $('#citys').val('Cualquier ciudad');
        return;
    }
    let prov = getProvinceByName(name);
    let c = await setCitys(prov.id);
    if(c) {
        setCiudades(orderArray(c, 'nombre'));
    }
}

function getProvinceByName(name) {
    if(name == 'Cualquier provincia') {
        return null
    }
    return locations.find(p => p.nombre == name);
}

const setCitys = async (id) => {
    const resCiudades = await fetch(urlApiCiudades + id + '&max=1000');
    const dataCiudades = await resCiudades.json();
    return filterCitys(dataCiudades.municipios);
}

function filterCitys(cits) {
    let filtereds = [];
    cits.forEach(c => {
        let finded = false;
        allProfessionals.forEach(pr => {
            if(pr.location && pr.location.city === c.nombre){
                finded = true;
            }
        });
        if(finded) {
            filtereds.push(c);
        }
    });
    return filtereds;
}

function setCiudades(citys) {
    ciudades = citys;
    $('#citys').removeClass('disabled');
    let html = '<option class="dropdown-item">Cualquier ciudad</option>';
    citys.forEach(c => {
        html += '<option class="dropdown-item">' + c.nombre + '</option>'
    })
    $('#citys').html(html);
}


// Fin de carga de ciudades



//carga de datos cuando se busca

function sendData() {
    let obj = {
        name: null,
        medicalInsurances: [],
        categories: [],
        radio: null,
        center: {
            lat: null,
            lng: null
        }
    }
    
    obj.name = getNameOfProfessional();

    if(obj.name == '') {
        obj.name = null
    }

    let insuranceSelected = getInsuranceIdByName($('#insurances').val());
    let categorySelected = getCategoryIdByName($('#categories').val());
    let locationSelected = getProvinceByName($('#locations').val());
    let ciudadSelected = getCityByName($('#citys').val());

    if(insuranceSelected && insuranceSelected != -1) {
        obj.medicalInsurances.push(insuranceSelected);
    }
    if(categorySelected && categorySelected != -1) {
        obj.categories.push(categorySelected);
    }
    if(locationSelected && locationSelected.id != -1) {
        obj.radio = 100;
        obj.center.lat = locationSelected.centroide.lat;
        obj.center.lng = locationSelected.centroide.lon;
    }
    if(ciudadSelected && ciudadSelected.id != -1) {
        obj.radio = 100;
        obj.center.lat = ciudadSelected.centroide.lat;
        obj.center.lng = ciudadSelected.centroide.lon;
    }
    sendAjax(obj);
}

function sendAjax(obj) {
    
    $.ajax({
      type: "POST",
      url: postUrl,
      data: JSON.stringify(obj),
      success: function(e) {
          if(e && e.result) {
              setProfessionals(orderArray(e.result, 'name'));
          }
          loading(false);
          localStorage.setItem('searchObj', null)
      },
      contentType: 'application/json',
      error: function (e) {
          console.error(e);
          loading(false);
          sendErrorToast(e);
      }
    });
}

function sendErrorToast(error) {
    $('.toast-agendapp').addClass('show');
    setTimeout(()=> {
        closeToast();
    }, 4000);
}

function closeToast(error) {
    $('.toast-agendapp').removeClass('show');
}

function getNameOfProfessional() {
    return $('#professional_name').val();
}

function parsePhoto(photo) {
    return photo ? photo : 'img/generic.png';
}

function gotoHome() {
    window.location.assign('/')
}


function getInsurances(p) {
    let ret = '<span style="color: #999"> SIN OBRAS SOCIALES </span>';
    if(p && p.length > 0) {
        ret = '<span class="insurance-span">';
        p.forEach((ins, index) => {
            ret += getInsuranceById(ins).name;
            if(index + 1 < p.length) {
                ret += ' &nbsp; ';
            }
        });
        ret += '</span>';
    }
    return ret;
}

function goProfile(id) {
    window.location.assign('perfil.html?uid=' + id);
}

function getCategory(uid) {
    return categories.find(c => c.uid == uid);
}

function getInsuranceById(uid) {
    return insurances.find(c => c.uid == uid);
}

function getMedicalInsurancesId(name) {
    let ret = [];
    insurances.forEach(m => {
        if(m.name.toLowerCase() == name.toLowerCase()) {
            ret = [ m.uid ];
            $('#insurances button').text(name);
            $('.hidden-filters').addClass('opened');
        }
    });
    return ret;
}

function getCenterOfLocation(name) {
    let ret = {lat: null, lng: null}
    locations.forEach(p => {
        if(p.nombre.toLowerCase() == name.toLowerCase()) {
            ret = { lat: p.centroide.lat, lng: p.centroide.lon}
        }
    });
    return ret;
}

function getCenterOfCity(name) {
    let ret = {lat: null, lng: null}
    ciudades && ciudades.forEach(c => {
        if(c.nombre.toLowerCase() == name.toLowerCase()) {
            ret = { lat: c.centroide.lat, lng: c.centroide.lon}
        }
    })
    return ret;
}

function getCategoryIdByName(cat) {
    if(cat == 'Todas las categorías') {
        return null;
    }
    let ret = categories.find(c => c.name.toLowerCase() == cat.toLowerCase());
    return ret.uid;
}

function getCityByName(cat) {
    if(cat == 'Cualquier ciudad') {
        return null;
    }
    return ciudades.find(c => c.nombre.toLowerCase() == cat.toLowerCase());
}

function getInsuranceIdByName(n) {
    if(n == 'Todas las obras sociales') {
        return null;
    }
    console.log(insurances)
    let ret = insurances.find(c => c.name.toLowerCase() == n.toLowerCase());
    return ret.uid;
}

$('#btn-search').click(()=> {
    loading(true);
    sendData();
});

$('.filters-search').on('keyup', function(e) {
    e.preventDefault();
    e.stopPropagation();
    if(e.keyCode == 13) {
        loading(true);
        sendData();
    }
})


$('#locations').on('change', function (e) {
    setProvince($(this).val())
})
