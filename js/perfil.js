const postUrl = "https://ms2.pragmadesarrollos.com/api/professionalsData/list";
const getUrl = "https://ms2.pragmadesarrollos.com/api/professionalsData/filters";
let profesionalAux;
let allInsurances;
let allCategories;
let profesional;

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

function orderArray(items, prop) {
    items.sort(function (a, b) {
      if (a[prop] > b[prop]) {
        return 1;
      }
      if (a[prop] < b[prop]) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
    return items;
}

const loadProfessional = async () => {
    let obj = {
        Id: findGetParameter('uid')
    }
    return new Promise((resolve, reject) => {
        $.ajax({
          type: "POST",
          url: postUrl,
          data: JSON.stringify(obj),
          success: function(e) {
              if(e && e.result) {
                  profesional = e.result[0];
                  resolve(true);
              } else {
                  reject(false);
              }
          },
          contentType: 'application/json',
          error: function (e) {
              console.error(e);
              reject(false);
          }
        });
    })
} 

const loadCategories = async () => {
    const res = await fetch(getUrl);
    const data = await res.json();
  
    console.error(data);

    return data;
    
}

function loading(b) {
    if(b) {
        $('body').addClass('loading');
    } else {
        $('body').removeClass('loading');
    }
}

function sendErrorToast(error) {
    $('.toast-agendapp').addClass('show');
    setTimeout(()=> {
        closeToast();
    }, 4000);
}

function closeToast(error) {
    $('.toast-agendapp').removeClass('show');
}

function parsePhoto(photo) {
    return photo ? photo : 'img/generic.png';
}

function gotoHome() {
    window.location.assign('/')
}

function setProfessionals(profs) {
    let html = '';
    setQtyProfs(profs.length);
    profs.forEach((p, i) => {
        if(p.approved) {
        html += `
            <div class="card-professional animated fadeInUp delay-${i + 1}">
			   <div class="left-content">
			      <div class="name-wrapper">
			        <div class="img-wrapper">
			           <img src="${parsePhoto(p.photo)}"  alt="">
			        </div>
			        <div class="name">
			           <h2>${p.name || 'Sin nombre'}</h2>
			           <p>${p.isCenter ? 'Centro' : (p.occupation || 'Sin profesión')}</p>
					   
			        </div>
			        </div>` + (p.location ? `
			        <div class="other-data">
			           <p>Dirección</p>
			           <h4>${p.location.address}, ${p.location.city}, ${p.location.country}</h4>
			           ${getInsurances(p.medicalInsurance)}
			         </div>
			       ` : ``) + `
                   <div class="actions-wrapper">
                    <button class="profile-btn" onclick="goProfile('${p.uid}');"  >Ver perfil</button>
                    <button class="turn-btn" onclick="login('${p.uid}');"  >Pedir turno</button>
                   </div>
				   </div>
			       <div class="right-content">
			          <div class="map-wrapper">
                       
                        ` + ( p.location ? `<iframe src="https://www.google.com/maps/embed/v1/place?q=${p.location.coord.lat},${p.location.coord.lng}&amp;key=AIzaSyBGv-LNZ5-OmrHeXKHRVbelojiSRQXO4O4" width="600" height="320" style="border:0;" allowfullscreen="" loading="lazy"></iframe> ` : 
						`<img src="img/mapa.jpg" width="600" height="100%" style="border:0;"></iframe> `) + `

                   

			  
			           </div>
			       </div>
			    </div>
        `;
        }
    });
    $('#cards').html(html);
    selFiltered();
}

function getInsurances(p) {
    let ret = '';
    if(p && p.length > 0) {
        ret += '<br><p>Obras sociales</p><h4>';
        p.forEach((ins, index) => {
            ret += getInsurance(ins).name;
            if(index + 1 < p.length) {
                ret += ', ';
            }
        });
        
        ret += '</h4>';
    }
    return ret;
}

function getCategory(uid) {
    return categories.find(c => c.uid == uid);
}

function getInsurance(uid) {
    return insurances.find(c => c.uid == uid);
}


function getMedicalInsurancesId(name) {
    let ret = [];
    insurances.forEach(m => {
        if(m.name.toLowerCase() == name.toLowerCase()) {
            ret = [ m.uid ];
            $('#insurances button').text(name);
            $('.hidden-filters').addClass('opened');
        }
    });
    return ret;
}

function getCenterOfLocation(name) {
    let ret = {lat: null, lng: null}
    setTimeout(()=> {
        console.log(locations)
    }, 3000)
    locations.forEach(p => {
        if(p.nombre.toLowerCase() == name.toLowerCase()) {
            ret = { lat: p.centroide.lat, lng: p.centroide.lon}
            $('#locations button').text(name);
            $('.hidden-filters').addClass('opened');
        }
    });
    return ret;
}

function getCenterOfCity(name) {
    let ret = {lat: null, lng: null}
    ciudades.forEach(c => {
        if(c.nombre.toLowerCase() == name.toLowerCase()) {
            ret = { lat: c.centroide.lat, lng: c.centroide.lon}
            $('#ciudades button').text(name);
            $('.hidden-filters').addClass('opened');
        }
    })
    return ret;
}

function getCategoryIdByName(cat) {
    let ret = [];
    categories.forEach(c => {
        if(c.name.toLowerCase() == cat.toLowerCase()) {
            ret.push(c.uid);
            $('#categories button').text(cat);
        }
    });
    return ret;
}



function setProfesional() {
    $('#name').html(profesional.name);
    $('#occupation').html(profesional.occupation);
    $('#description').html(profesional.description || 'No hay ninguna descripción para mostrar');
    $('#address').html(profesional.location.address + ', ' + profesional.location.city);
    $('#photo').attr('src', profesional.photo);
    setMap();
    setInsurance();
}

function setMap() {
    $('#map').html(`<iframe src="https://www.google.com/maps/embed/v1/place?q=${profesional.location.coord.lat},${profesional.location.coord.lng}&amp;key=AIzaSyBGv-LNZ5-OmrHeXKHRVbelojiSRQXO4O4" width="100%" height="400" style="border:0;" allowfullscreen="" loading="lazy"></iframe>`)
}

function getData() {
    loading(true);
    loadCategories().then( d => {
        allInsurances = d.result.medicalInsurances;
        allCategories = d.result.categories;
        loadProfessional().then(p => {
            console.warn(profesional);
            setProfesional();
            loading(false);
        });
    });
    
}

function loading(b) {
    console.error(b)
    if(b) {
        var preloader = $('#loading'),
			loader = preloader.find('#loading-center-absolute');
			loader.show();
			preloader.fadeIn('fast');
    } else {
        var preloader = $('#loading'),
			loader = preloader.find('#loading-center-absolute');
			loader.fadeOut();
			preloader.delay(400).fadeOut('slow');
    }
}

function setInsurance() {
    if(profesional.medicalInsurance) {
        profesional.medicalInsurance && profesional.medicalInsurance.forEach( i => {
            let ins = getInsuranceById(i);
            $('#insurances').append('<span class="insurance">' + ins.name + '</span>')
        })
    } else {
        $('#insurances').append('No hay ninguna obra social para mostrar')
    }
    
}

function gotoMain() {
    window.location.assign('/index.html');
}

function getInsuranceById(id) {
   return allInsurances.find(p => p.uid == id);
}

function setCategory(id) {
    return allCategories.find(p => p.uid == id);
}

$('.toggle-menu-close').click( function() {
    $('.links-wrapper').removeClass('opened');
});


function openMenu() {
    $('.links-wrapper').addClass('opened');
}

function closeMenu() {
    $('.links-wrapper').removeClass('opened');
}

function pedirTurno() {
    login(profesional.uid, profesional);
}

getData();

