
var agendappUser = null;
var fbUser = null;
var professionalSelected = null;
//var apiUrl = "http://localhost:5000/api";
var apiUrl = "https://ms2.pragmadesarrollos.com/api";

function pad(s) { return (s < 10) ? '0' + s : s; }
var turnModal = new bootstrap.Modal(document.getElementById('turnModal'));
var okModal = new bootstrap.Modal(document.getElementById('okModal'));
var failModal = new bootstrap.Modal(document.getElementById('failModal'));

var celularErrorModal = new bootstrap.Modal(document.getElementById('celularErrorModal'));



var firebaseConfig = {
	apiKey: "AIzaSyDhHIxgYhYU00Mr_PhxHdpJi-bmqCd7xyc",
	authDomain: "busca-fc0c0.firebaseapp.com",
	databaseURL: "https://busca-fc0c0.firebaseio.com",
	projectId: "busca-fc0c0",
	storageBucket: "busca-fc0c0.appspot.com",
	messagingSenderId: "55250584249",
	appId: "1:55250584249:web:63c0f25d49ef8d71115168"
};

firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged(async (user) => {
	console.log(user);
	agendappUser = user;



	if (user) {
		$("#btnLogout").css("display", "inherit");
		var uid = user.uid;
		fbUser = await getUser(uid);

		if (fbUser && fbUser.Phone) {
			$("#txtInputCelular").val(fbUser.Phone);
		}

	} else {
		$("#btnLogout").css("display", "none");
	}
});

function login(uid, prof) {

	professionalSelected = prof || allProfessionals.find(p => p.uid == uid);
	if (agendappUser != null) {
		gotoCreateTurn();
		return;
	}
	var provider = new firebase.auth.GoogleAuthProvider();
	firebase.auth().signInWithPopup(provider)
		.then((result) => {
			var credential = result.credential;
			var token = credential.accessToken;
			var user = result.user;
			// ...
			setTimeout(async () => {
				//UidOneSignal
				var us = fbUser = await getUser(agendappUser.uid);
				if (!us || us.UidOneSignal == null || us.UidOneSignal == '') {
					updateUser({
						Name: agendappUser.displayName,
						Surname: "",
						Email: agendappUser.email,
						Uid: agendappUser.uid,
						IsWeb: true
					});
					/* Load Celular */
					if (us && us.Phone) {
						$("#txtInputCelular").val(us.Phone);
					}
					if (!fbUser) {
						fbUser = await getUser(agendappUser.uid);
					}
				}

			}, 10);
			gotoCreateTurn();
		}).catch((error) => {
			var errorCode = error.code;
			var errorMessage = error.message;
			var email = error.email;
			var credential = error.credential;
		});
}

function logout() {
	firebase.auth().signOut().then(() => {

	}).catch((error) => {

	});
}


function gotoCreateTurn() {
	console.log("gotoCreateTurn");
	const bProfessional = $("#bProfessional");

	const selectLocation = $("#selectLocation");
	const selectTurnType = $("#selectTurnType");

	const ddlSelectLoction = $("#ddlSelectLoction");
	const ddlSelectTurnType = $("#ddlSelectTurnType");

	if (professionalSelected.typesTurns && professionalSelected.typesTurns.length > 0) {
		selectTurnType.css("display", "inherit");
		ddlSelectTurnType.html('');
		professionalSelected.typesTurns.forEach(p => {
			ddlSelectTurnType.append(`<option value="${p.name}">${p.name}</option>`)
		});
	} else {
		selectTurnType.css("display", "none");
		ddlSelectTurnType.html('');
	}

	if (professionalSelected.moreLocation && professionalSelected.moreLocation.length > 0) {
		selectLocation.css("display", "inherit");
		ddlSelectLoction.html('');
		const loc = professionalSelected.location;
		const dir1 = (loc.description || '') + ' (' + loc.address + ', ' + loc.city + ', ' + loc.province + ', ' + loc.country + ')';
		ddlSelectLoction.append(`<option value="${dir1}">${dir1}</option>`);
		professionalSelected.moreLocation.forEach(p => {
			const dir = p.description + ' (' + p.address + ', ' + p.city + ', ' + p.province + ', ' + p.country + ')';
			ddlSelectLoction.append(`<option value="${dir}">${dir}</option>`);
		});
	} else {
		selectLocation.css("display", "none");
		ddlSelectLoction.html('');
	}

	bProfessional.html(professionalSelected.name);
	turnModal.show();
	console.log(professionalSelected);
}

function createTurn() {
	console.log("createTurn");

	if (!professionalSelected || !professionalSelected.name || !professionalSelected.uid) {
		console.error('professional cannot be null');
		return;
	}

	if (!agendappUser || !agendappUser.displayName || !agendappUser.uid || !agendappUser.email) {
		console.error('agendappUser cannot be null');
		return;
	}

	var txtInputCelular = $("#txtInputCelular").val();
	if (!txtInputCelular || txtInputCelular.length != 10) {
		console.error('Debe ingresar un número celular válido');
		celularErrorModal.show();
		return;
	}

	var daysCheck = $("#daysCheck").prop('checked');
	var dateCheck = $("#dateCheck").prop('checked');
	var hoursCheck = $("#hoursCheck").prop('checked');


	var txtPickerStart = $("#txtPickerStart").val();
	var txtPickerEnd = $("#txtPickerEnd").val();

	var ddlHI = $("#ddlHI").val();
	var ddlHF = $("#ddlHF").val();

	var selLocation = null;
	var selTurnType = null;
	var selectLocation = $("#selectLocation");
	var selectTurnType = $("#selectTurnType");
	var ddlSelectLoction = $("#ddlSelectLoction");
	var ddlSelectTurnType = $("#ddlSelectTurnType");
	if (selectLocation.css('display') === 'inherit' || selectLocation.css('display') === 'block') {
		selLocation = ddlSelectLoction.val();
	}
	if (selectTurnType.css('display') === 'inherit' || selectTurnType.css('display') === 'block') {
		selTurnType = ddlSelectTurnType.val();
	}

	var data = {
		blocked: false,
		canceled: false,
		clientConfirmed: false,
		clientName: `${professionalSelected.name} - ${professionalSelected.occupation}`,
		clientPhoto: professionalSelected.photo,
		clientCategory: professionalSelected.categorie,
		clientSeeNoti: true,
		clientUid: professionalSelected.uid,
		dateFrom: {
			__type: "Date",
			//iso: "2021-08-09T08:00:00.000Z"
		},
		dateTo: {
			__type: "Date",
			//iso: "2021-08-09T20:00:00.000Z"
		},
		moreInfo: {
			Have: true,
			DataLocation: selLocation,
			/*DateFrom: "09/08/2021",
			DateTo: "16/08/2021",
			Days: {
				Su: false,
				Mo: true,
				Tu: true,
				We: true,
				Th: true,
				Fr: true,
				Sa: true
			},
			From: "08:00",
			To: "20:00"*/
		},
		observation: `${agendappUser.displayName} acaba de solicitar un turno desde la web`,
		userConfirmed: true,
		userName: agendappUser.displayName,
		userSeeNoti: true,
		userUid: agendappUser.uid,
		userEmail: agendappUser.email,
		turnType: selTurnType,
		userPhone: txtInputCelular,
		userPhoto: agendappUser.photoURL
	};

	if (daysCheck) {
		var btnLu = $("#btnLu").prop('checked');
		var btnMa = $("#btnMa").prop('checked');
		var btnMi = $("#btnMi").prop('checked');
		var btnJu = $("#btnJu").prop('checked');
		var btnVi = $("#btnVi").prop('checked');
		var btnSa = $("#btnSa").prop('checked');
		var btnDo = $("#btnDo").prop('checked');
		data.moreInfo.Days = {
			Su: btnDo,
			Mo: btnLu,
			Tu: btnMa,
			We: btnMi,
			Th: btnJu,
			Fr: btnVi,
			Sa: btnSa
		};
	}

	var txtPickerStart = $("#txtPickerStart").val();
	var txtPickerEnd = $("#txtPickerEnd").val();
	if (dateCheck) {
		data.moreInfo.DateFrom = txtPickerStart;
		data.moreInfo.DateTo = txtPickerEnd;
	}

	var ddlHI = $("#ddlHI").val();
	var ddlHF = $("#ddlHF").val();
	if (hoursCheck) {
		data.moreInfo.From = ddlHI;
		data.moreInfo.To = ddlHF;
	}

	if (!txtPickerStart || txtPickerStart.indexOf('/') == -1 || txtPickerStart.split('/').length != 3 ||
		!txtPickerEnd || txtPickerEnd.indexOf('/') == -1 || txtPickerEnd.split('/').length != 3) {
		var dti = new Date();
		txtPickerStart = [pad(dti.getDate()), pad(dti.getMonth() + 1), dti.getFullYear()].join('/');
		var dtf = new Date();
		dtf.setTime(dti.getTime() + 604800000);
		txtPickerEnd = [pad(dtf.getDate()), pad(dtf.getMonth() + 1), dtf.getFullYear()].join('/');
	}

	var pd1 = txtPickerStart.split('/');
	var pd2 = txtPickerEnd.split('/');
	data.dateFrom.iso = `${pd1[2]}-${pd1[1]}-${pd1[0]}T${ddlHI}:00.000Z`;
	data.dateTo.iso = `${pd2[2]}-${pd2[1]}-${pd2[0]}T${ddlHF}:00.000Z`;

	console.log(data);

	var btnCreateTurn = $("#btnCreateTurn");
	btnCreateTurn.prop('disabled', true);
	$.ajax(`${apiUrl}/turns/create`, {
		data: JSON.stringify(data),
		contentType: 'application/json',
		dataType: 'json',
		type: 'POST',
	}).done(function (data, textStatus, jqXHR) {
		console.log('create turn ok', data);
		btnCreateTurn.prop('disabled', false);
		turnModal.hide();
		if (data.result) {
			/* Load Celular To Firebase */
			setTimeout(async () => {
				if (!fbUser || fbUser.UidOneSignal == null || fbUser.UidOneSignal == '') {
					if (!fbUser) {
						fbUser = await getUser(agendappUser.uid);
					}
					fbUser.Phone = txtInputCelular;
					fbUser.IsWeb = true;
					updateUser(fbUser);
				}
			}, 1);
			okModal.show();
			return;
		}
		failModal.show();
	}).fail(function (jqXHR, textStatus, errorThrown) {
		console.error('update user err', textStatus + " " + errorThrown);
		btnCreateTurn.prop('disabled', false);
	});


}

function updateUser(user) {
	$.ajax(`${apiUrl}/users/update`, {
		data: JSON.stringify(user),
		contentType: 'application/json',
		dataType: 'json',
		type: 'POST',
	}).done(function (data, textStatus, jqXHR) {
		console.log('update user ok', data);
	}).fail(function (jqXHR, textStatus, errorThrown) {
		console.error('update user err', textStatus + " " + errorThrown);
	});
}

function getUser(uid) {
	return $.ajax(`${firebaseConfig.databaseURL}/Users/${uid}.json`, {
		dataType: 'json',
		type: 'GET',
	}).done(function (data, textStatus, jqXHR) {
		console.log('get user isweb ok', data);
		return data;
	}).fail(function (jqXHR, textStatus, errorThrown) {
		console.error('get user isweb err', textStatus + " " + errorThrown);
	});
}

function onChangeCheck(check, div) {
	var jqelem = $(`#${div}`);
	var display = jqelem.css('display');
	jqelem.css('display', display == 'none' ? 'inherit' : 'none');
}




var dti = new Date();
var sdti = [pad(dti.getDate()), pad(dti.getMonth() + 1), dti.getFullYear()].join('/');

var dtf = new Date();
dtf.setTime(dti.getTime() + 604800000);
var sdtf = [pad(dtf.getDate()), pad(dtf.getMonth() + 1), dtf.getFullYear()].join('/');


$('#txtPickerStart').val(sdti);
$('#txtPickerEnd').val(sdtf);

$('#datePickerStart').datepicker({ language: 'es', format: 'dd/mm/yyyy' });
$('#datePickerEnd').datepicker({ language: 'es', format: 'dd/mm/yyyy' });


